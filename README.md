# django-cross-site-urls

[![Latest Version](http://img.shields.io/pypi/v/django-cross-site-urls.svg?style=flat-square)](https://pypi.python.org/pypi/django-cross-site-urls/)

[![License](http://img.shields.io/pypi/l/django-cross-site-urls.svg?style=flat-square)](https://pypi.python.org/pypi/django-cross-site-urls/)

Please consider that this module is in active development and is currently not ready for production.

*A Django module allowing to resolve urls across two django different sites.*

## Requirements

Python 3.3+, Django 1.11+, Django REST framework 3.2+, Slumber 0.7+.

## Installation

Just run:

```sh
pip install django-cross-site-urls
```

Once installed you just need to add `cross_site_urls` to `INSTALLED_APPS` in your project's settings module:

```py
INSTALLED_APPS = (
    # other apps
    'rest_framework',
    'cross_site_urls',
)
```

Add module configuration to urls of the two django sites:

```py
from cross_site_urls.conf import settings as cross_site_settings
```

```py
urlpatterns = i18n_patterns(
  # other urls patterns
  url((r'^{}').format(cross_site_settings.DEFAULT_API_URL), include('cross_site_urls.urls')),
)
```

Configure required `CROSS_SITE_URLS_SITES` settings

```py
CROSS_SITE_URLS_SITES = {
    'site_a': {
        'domain': 'a.example.com',
        'scheme': 'https',
     },
    'site_b': {
        'domain': 'b.example.com',
        'scheme': 'http',
     },
}
```

*Congrats! You’re in.*

## Authors

Kapt <dev@kapt.mobi> and [contributors](https://github.com/kapt-labs/django-cross-site-urls/contributors)

## License

BSD. See `LICENSE` for more details.
