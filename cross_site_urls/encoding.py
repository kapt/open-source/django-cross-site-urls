def prefix_kwargs(prefix, kwargs):
    prefixed_kwargs = {}
    for k, v in kwargs.items():
        prefixed_key = f"{prefix}_{k}"
        prefixed_kwargs[prefixed_key] = v
    return prefixed_kwargs


def unprefix_kwargs(prefix, kwargs):
    unprefixed_kwargs = {}
    for k, v in kwargs.items():
        if k.startswith(prefix):
            prefix_len = len(prefix)
            unprefixed_size = prefix_len + 1
            unprefixed_key = k[unprefixed_size:]
            unprefixed_kwargs[unprefixed_key] = v
    return unprefixed_kwargs
