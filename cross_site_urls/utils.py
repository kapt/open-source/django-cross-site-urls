# Django
from django.urls import reverse

# Local application / specific library imports
from .constants import RESOLVE_API_VIEW_URL


def get_api_url(scheme, domain):
    resolve_view_url = reverse("resolve-url").replace(RESOLVE_API_VIEW_URL, "")
    return "{}://{}{}".format(scheme, domain, resolve_view_url)
