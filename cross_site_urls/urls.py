# Django
from django.urls import path

# Local application / specific library imports
from .constants import RESOLVE_API_VIEW_URL
from .views import URLResolveAPIView

urlpatterns = [
    path(RESOLVE_API_VIEW_URL, URLResolveAPIView.as_view(), name="resolve-url"),
]
