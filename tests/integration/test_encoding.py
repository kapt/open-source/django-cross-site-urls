# Standard Library
import uuid

# Project
from cross_site_urls.encoding import prefix_kwargs, unprefix_kwargs


class TestPrefixUnPrefixKwargs:
    def test_prefix_kwargs_can_be_unprefixed(self):
        kwargs = {"key1": "value1", "key2": "value2"}
        prefix = str(uuid.uuid4())
        prefixed_kwargs = prefix_kwargs(prefix, kwargs)

        unprefixed_kwargs = unprefix_kwargs(prefix, prefixed_kwargs)
        assert kwargs == unprefixed_kwargs
