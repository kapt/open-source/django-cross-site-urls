# Django
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path

# Project
from cross_site_urls.conf import settings as cross_site_settings

admin.autodiscover()

urlpatterns = i18n_patterns(
    "",
    re_path(
        (r"^{}").format(cross_site_settings.DEFAULT_API_URL),
        include("cross_site_urls.urls"),
    ),
)

urlpatterns += staticfiles_urlpatterns()
